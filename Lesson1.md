
[[_TOC_]]

# COMPUTERS & DATA 
Computers are made of two parts: hardware: anything you can kick, software all the rest that you can just yell at. 

## Hardware

Our computers are based mostly on von Neumann architecture. (Please check [John von Neumann](https://en.wikipedia.org/wiki/John_von_Neumann) biography)

![vonNeumann Architecture, taken from Wikipedia](/home/fex/Documents/Praga/Lezioni/Clean/im_l1/cpWeZaU.png)

[^]: Image taken from https://commons.wikimedia.org/wiki/File:Von_Neumann_Architecture.svg. This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

We do not focus too much into hardware (HW), so just a basic knowledge is needed. 



---

## Software

There are different types of software, each one with a specific purpose:

1. Firmware: Is a permanent software and it is deputed to interact with the HW

2. Operating System (OS): is the interface between the User and the HW

3. Dedicated software: those are application that run within the operating system. They do not need to interface directly with the HW since they exploit the OS (e.g. using drivers or libraries)
    The OS is divided in several parts

  1. Kernel

  2. Networking
  3. Security
  4. UI/GUI

**Take Home Message** : be always aware if you talk about HW, SW, OS, Kernel and which flavour you are
using.



## Kernel

Kernel means *core*, the MOST IMPORTANT part of the OS. The Kernel handles the boot, the RAM, the CPU, the peripherals (driver), programs, etc. In other word is the part of the OS that allows to use the HW. The kernel also specify the filesystem (fs), which is the structure used to save data.

### Popular Kernels

1. Windows NT (hybrid types: some extra function)
2. **UNIX**
3. Berkeley Software Distribution (derived from Unix, the base of Apple Kernel)

**IMPORTANT** Linux is an operating system BASED on one specific Linux UNIX-based kernel. Same goes for Mac (MacOS) which is based on BSD, which is based on UNIX.

 This is the tree of Unix evolution over time take from Wikipedia

![Unix history tree](/home/fex/Documents/Praga/Lezioni/Clean/im_l1/1024px-Unix_history-simple.svg.png)

[^]: Image taken from https://commons.wikimedia.org/wiki/File:Unix_history-simple.svg . This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.



### UNIX-Based Linux

Linux is free open-source, monilithic, Unix-linux kernel. It is available on github [linux](https://github.com/torvalds/linux)
On top of the Linux-Kernel is possible to build several LINUX-Distro (distributions). There are thousands of Linux Distro, But they are mostly divided families accordingly to which kernel they are based on. 

Here some of the main OS and some derived ones:

1. Debian (most used I would say) - *Ubuntu, Xubuntu, Kubuntu, Mint, MATE*

2. Slackware (Oldest Distro still mainteined (i.e. alive) - The most UNIX like) -  *Suse, OpenSUSE*

3. RedHat (most professional UNIX) - *Fedora* (free), *RedHat* (OPEN, NOT FREE!)

   

In the last two decades the number of available Linux Distro exploded. A (I think) comprehensive list of distros is available [here](https://en.wikipedia.org/wiki/List_of_Linux_distributions#/media/File:Linux_Distribution_Timeline_27_02_21.svg) The image is huge but it is in the `.svg` format so you can search within it.

A small reproduction of it to give an idea of the magnitude of it:



![Linux Tree](/home/fex/Documents/Praga/Lezioni/Clean/im_l1/Linux_Distribution_Timeline_27_02_21.svg.png)

[^]: Image taken from https://commons.wikimedia.org/wiki/File:Linux_Distribution_Timeline_27_02_21.svg. Data available at https://github.com/FabioLolix/LinuxTimeline/tree/master *If this file is [eligible for relicensing](https://commons.wikimedia.org/wiki/Commons:GFDL_1.3_relicensing_criteria), it may also be used under the [Creative Commons Attribution-ShareAlike 3.0](https://creativecommons.org/licenses/by-sa/3.0/) license.* The relicensing status of this image has not yet been reviewed.  [You can help](https://commons.wikimedia.org/wiki/Commons:License_Migration_Task_Force/Migration).

 **THM** : This slide introduces GIT, open and free source concepts.

---

## FileSystems (fs)
fs are structures rather than code, hw or fw. It is formed of files, folder, and how they are interconnected. It
also contains info on how to retrieve files from the HW.

1. Part of the O.S. (kernel) or Data support
2. Data-Interface for user
3. Depends on the media (fs of BlueRay Disk is different from the fs of a tape memory)
4. Example: max number of files/folder depends on fs
5. Linux file sytem: ext4. is one of the most powerful; e.g. CASE sEnSITive

**THM** : When we deal with DATA we are dealing with an Operating System, which stores/manages data in
different ways. Different fs have different restrictions.

### QUESTIONS - block A

1. What's the difference between Xubuntu, Kubuntu and Ubuntu?

2. What OS is running on your machine?

3. May I use UNIX as OS?

   ---

   

## Different File structures for different OS
### Windows
WNT is ubiquitous, so it can not be omitted, but we MOSTLY USE LINUX. Nonetheless I report here the FILE
STRUCTURE for a standard Windows OS
The table is taken verbatim from [Wikipedia](https://en.wikipedia.org/wiki/Directory_structure#Windows_10):
**Folder**: Description
**\PerfLogs**: May hold Windows performance logs, but on a default configuration, it is empty.
**\Program Files**: 32-bit architecture: All programs (both 16-bit and 32-bit) are installed in this folder. 64-bit architecture: 64-bit programs are installed in this folder.
**\Program Files (x86)**: Appears on 64-bit editions of Windows. 32-bit and 16-bit programs are by default installed in this folder, even though 16-bit programs do not run on 64-bit Windows.[3]
**\ProgramData (hidden):** Contains program data that are expected to be accessed by computer programs regardless of the user account in the context of which they run. For example, an program may store specific information needed to operate DVD recorders or image scanners connected to a computer, because all users use them. Windows itself uses this folder. For example, Windows Defender stores its virus definitions in `\ProgramData\Microsoft\Windows Defender`. Programs do not have permission to store files in this folder, but have permission to create sub-folders and store files in them. The organization of the files is at the discretion of the developer.
**\Users**: User profile folders. This folder contains one subfolder for each user that has logged onto the system at least once. In addition, it has two other folders: `Public` and `Default` (hidden). It also has two folder like-items called `Default User` (an NTFS junction point to `Default` folder) and `All Users` (a NTFS symbolic link to `C:\ProgramData`).
**\Public**: This folder serves as a buffer for users of a computer to share files. By default this folder is ccessible to all users that can log on to the computer. Also, by default, this folder is shared over the network, although anonymous access (i.e. without a valid password-protected user account) to it is denied.
This folder contains user data, not program data, meaning that users are expected to be sole decider of what is in this folder and how it is organised. It is unethical for a program to store its proprietary data here. (There are other folders dedicated to program data.)

### Linux/UNIX folder structure

The UNIX-fs has been the first bit of code to be developed and it is CENTRAL to the OS. Linux deals with HW and SW through the use of files. There are 3 types of files:

1. regular files (chunk of data)

2. folders (collective file)

3. device files (contained in `/dev`)
   The last type of files is quite complex, but basically means that some HW is stored as a special file in the fs.
   This is a scheme of main folder on a Unix system

   ![Unix tree](/home/fex/Documents/Praga/Lezioni/Clean/im_l1/Standard-unix-filesystem-hierarchy.svg.png)

   [^]: Image taken from https://commons.wikimedia.org/wiki/File:Standard-unix-filesystem-hierarchy.svg. This file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons)[Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.

   More information [here](https://www.howtogeek.com/117435/htg-explains-the-linux-directory-structure-explained/) and [here](https://en.wikipedia.org/wiki/File_system) and specifically on [Unix](https://en.wikipedia.org/wiki/Unix_filesystem#Conventional_directory_layout)


   ## FILES
   ### Files (ordinary) type
   In general we consider file a chunk of DATA. That is not always true ( we just mentioned device files):

   - Executable files = contains commands to execute (example: executable, files that can be executes) They
     do not need any software to be open. They are dealt by the OS.

   - Data = contains info. Need an app to interpret the information that can be store

     

   The data can be store in two main ways

   - **binary format** = Written for a program, not for a human. In general executable files are binary. But even
     .jpg
   - **ASCII** = the data are human readable as a sequence of characters. (e.g. in vasp INCAR, OUTCAR, submission.script)

**OUR GOAL HERE IS TO USE MOSTLY ASCII FILES**

1. Independent from OS

2. Easy to edit, modify, search into, copy, compress, send, version independent, ...

3. Less nice to watch

   

   #### Questions - block B

1. Are figures ASCII? If NOT, what figures can be substitute by ASCII and which can not?
   Advantages/Disadvantages?
2. What's the difference between a `.DOC` file and a `.DOCX` file? and the difference with an `.ODT`?
3. Is `PDF` a ASCII? Is a `.SVG`? What advantages/disadvantages they have compared to a plain ASCII or a Binary?





[Notes for Digital-Age tools for researcher ](https://gitlab.com/cippo1987/DigitalageTools) © 2021   by  [ Federico Brivio ](https://gitlab.com/cippo1987/) is licensed under [ CC BY-NC 4.0 ![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxxJREFUSIm11luIVlUUB/DfzKhTwQQW2nQhiHwJAiOYyKjEKGhSoyCyh4QIulHQjSBf6sGp6AKVURBERUlJBBH1EPSQhVA9dBG6aPegtBG6TdNUjs7Xw97Hs77t+Zzv0/EPh3PO3v+9/nuvtfdau093OBIrsBpLcRyG0cIvGMeneBOb8U+XdjtiGI9jMot080ziCZxwsKJr8UeD4b/wJd6VVvcFJhp4E7iuF8E+3F8Y+ROP4HzMaxgzgHPxcMNkn8L8bkRfDINm8CQW9TDxY/Eo9gY7b80mfm8g/4urexAsMap99c92Il4mrbCF/7D8EEQrnKE9/teXhCPwYyD0tClmwSj2ZLu/Y2HsvD2IvjSHohU2BPtjVeOAlAQqF59yGIQXq10+gcF5WCZlItiI74tBi3ANTscUPsImKVFUOBNrpKSxS9rFb4f+XdLmuhVD8v55SO2G0UJ0GX61f3L4BksyZ516U8ZnE/qDrRWh7zFSBmrhbwwG4lHYoU6D9+Au/JyF1uPCYGwrbpLiuUdy6dnB3nx16t0CX+Wf7cVqrwpGbwvtSzGSv1/J/dM4KXAu1Zx0tmf+16Tc28I7BWl9EB7RjM/Vru8GmzN/MsagryC1wne/ZrRm6S9Racz0Y2f+Ob4gRddfEL5PxLVSsdiW207GqYGzSnt8KyzO73F4L898SspgFYakY1Dl7THcgM9y29NYqQ7HNtws5fup3HZRsDegPstbSKWsGryymOFyKc2VR+UD9ea5r6F/RiqtEeeE/g1wXmh4roN77sTzUolcY/8SN4IH8AIelBJKiVjjL65cMJ4bdqsTw1xiIX5T32D2hfSOMJtXD4NwDGdbCBbg29B5yxyKlmXxmJKwSn1dmc7/h4qztF8EbuxEXBdI01I1OVisle7Xlb2NByL34RntR+N1nNaD4BKpMkUbr0nhnBV3a78lTuNlXImjG/hDuEJa1W7t53lM9ykVXILvipm38oR24hN8LJXOvQ28H3B5L4IRC6SSWF2Nunl25DGDDfb2oaxInTAg3UZWS8l/WCoWLfyUJ/Y+3sCHkosPiP8Btp0swL9OhkEAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAArlJREFUSIm11k9oHVUUBvBfX2Ka11TbGkhaWwPu3FmoQiBBsXShNEEEC9KFkIVIN64sKAUXhVaolJZIIkZB6ap0kYUKpSstrbpw4UJqu1RjSlOMljTEpK2NizvDu5k3M5kXkw8uM9z7nfPdc/+cezaphjr2YwjPoBc7sYxbmMENfIMvMVfRbyG24wPMJyJV2gLGsWetoiOYzXE8j+u4jG/xixBhHu9IK4JtOJNxMofTeAHtBTaDOIW/M7af4JHVRGuYjIweCsvW08LEu5OJ/xv5ubSa+KmI/A9eL+A9i8+Ttq+AM2zlFnxcJHo4Ii0Ky5qHTbgZcaeTvjzszYi/lSVsFa5FShgpmh0e03yQHi3hD2ss+xx2x4PvR07OlThJcS3i/1yBHx/Wj9LODtzRuINPVnDUg7eTVuXgdUca86i3C3u5LSF8gakSB8fxdKZvMPn+JCSbPMziU7yDLrwIoxrLcKBEtK55b7OtXmL/fMQbg6saS9BRYthVQbirxL4ddxPedzXsSgamcK/E8P/igXD1oLcmvDSE67TRSDV21oTQKU4C64lUY7kmvKWE93Wjka7uTE0j/D3KE/mSkPImcsYmkrGlEvs2PJH834azql2nFIc0n+ZDFewGIv54DV9Hg69WcLBWDEf/Fwl39y+Np7BvFQdrifjxSGMBW2rC3f0wIXTiZNUQWsAx7Ej+xxJxhFT3u0YEZbVSqxEfFJLHslAWdWcJr2i8m/fw8joID1hZob5ZNLtjEem+/Mj7cSHT+nN4bwhnJvU3WiRKyCyfZaI5j6fKjDLoE4qJ2Mek/Oq0SfxdK6vEReE9PSj/6duMl4RSNo7yIU4I1WtlDOE3zfu5mPT/gO/xa0YsbX/gtVYEY3TiKP7McVzUZvEetpQ5rvoitQsVxBCeE5J9mnenhdz7I77CFeH6lOI/baALunnzplQAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxRJREFUSIml1luoZ3MUB/DPueaMQ0PkKLc8eOWBooTIA2eGkii8jHKb4kGMePCiyWAkU2Z4QEnydMp40ESMa4ohI3XSKJcZuR3jcsxw5jh/D2vv8//tfX577/+MVb/6//de67vWXr/vugwZTCZwKdbgbJyEKfTwA37ELN7CdvwxIG6jrMbDmC+cDHIOYCtOOVKn6zCXAZ7DbuzAa/gMP2f05nHH4TgcwRM1kN+wCedjOGMzhPPwEH6p2T6DsS6nw5hJjJbwJE44jMBX4xEsJjg7upw/migfxHUtulM4Uz4DcCV+T/CebgK6IVH6Gxe1RYhXCt3JFp1zBMNL3BV3PinKolRY1+F0UMcwrZ/2P9XY/mDi9Fdcj9EMyDCuwcuFXg/v4HFR302yOcF/qnw4LlhbL4evcTeOLfRGCodN9buEexocH58E+pdoSC5vAesJgmzGhtrzb/BuAvg5Tmz56k2J7TRsqT3YgO86gnk+ARzHraKNtsmFif1WeE//4staGxMs/7jB8SyuFekfVEYKHz28D3uKP7sbDC4Rjf/fTABf4S7dzC5ltrDbI4nizQ6js0SKcgNjv2g+p3Zg7NTP7rLjnQNGfRzuw95MAAt4Cec22L6uP0CWUz07oONSxnETPs0E0MPbuFq1pe7Sv6Jlcs3rniI5Mh0jf//l+RLrBQ/K7H5AdQRe1uJ0leDBNtV6vSCxX8Jz8jxIB8Y2YqVZ0c5qMoE3Er1DIsUfFr/T8Uc7D3pYS6S33DQO4rSM41FsxD8NQD18YWUTGcON+CTRO1BkD9V2+GLDV8PpeEBMpnLTmMFtgmxNkg6Jx9IXE/g2eXl7C0gp2wvdozv0rtAfi/vFwKjIGn12LhQG/9fxxapEu6VJMU35IdzZAjopSDTU8P5mVU5sacFCUD0lzYxol4PKGaJ71TFyi8UKuVe1KSzgBbF95AbCKlyFZ8W+ltb1Rs0LYVamxbCvl80ivhd1vAv7VFfZ8uwVo/OI5Chx7/Ulve3M4X5JreakiRh1GRXr7loxeaZwcvFuH37CR3hVrEOLXYD/AYTjaOgDscSLAAAAAElFTkSuQmCC)](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)

Your rights are available [here](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1).
