[[_TOC_]]

# Linux

# Commands
Set of basic commands I assume people know:
`pwd`, `ls`, `cd`, `mkdir`, `rm`, `man`, `cat`, `touch`, `ps`, `top`, `head`, `tail`, `mv`, `more`, `less`, ...

if not check some online guide or some O'Reilly Books

![bash](/home/fex/Documents/Praga/Lezioni/Clean/im_l2/9781491941591.jpg)

## Why Bash? (or any other shell?)

When we deal with 10s, 100s, 1000s files, it is **impossible** to deal by hand. Script not just makes everything:
1. faster
2. **reproducible**
often makes tasks
3. doable!
There are operations that can be done only by scripting.  

Some background story:
- Written by [Brian J. Fox](https://en.wikipedia.org/wiki/Brian_Fox_(computer_programmer)) 
- Unix Shell 
- Open source
-

![Brian Fox](/home/fex/Documents/Praga/Lezioni/Clean/im_l2/330px-BrianJFox.png)

[^]: Image taken from  https://commons.wikimedia.org/wiki/File:BrianJFox.png, Author Lissa Ligget; this file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons)[Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.



Supercoooool guy who came from a family of artists. 

- Usually default shell to Linux, now present in W10 (so popular that MS uses it)

- Brace  expansion (line that start with $ are input, output otherwise):

  ```bash
  $ echo a{1,2,3,4}
  a1e a2e a3e a4e
  $ echo {a,b,c}{01,2,3}
  a01 a2 a3 b01 b2 b3 c01 c2 c3
  $ echo {a..z..3}
  a d g j m p s v y
  ```

  

- auto-completion
- starting script `~/.bashrc`, `~/.profile`, `~/.config`, ...
- From wikipedia List of scripts:
Bash reads and executes `/etc/profile` (if it exists). (Often this file calls `/etc/bash.bashrc`)

After reading that file, it looks for `**~/.bash_profile`,`~/.bash_login`, and `~/.profile` in that order, and reads and executes the first one that exists and is readable.

When a login shell exits
Bash reads and executes `~/.bash_logout` (if it exists).

When started as an interactive shell (but not a login shell)
Bash reads and executes `/etc/bash.bashrc` and then `~/.bashrc` (if it exists).


## Bash not the only one
- Bourne shell
- tcsh
- csh
- power shell

### Key term
- POSIX: someone the standard to convert human typed commands and computer commands across systems.
- Cygwin is an environment for windows where POSIX can be used. Nothing against it, but it is literary using a bad version of something, on something that is not even design to do it. Also you increase the complexity and the number of options to know. But you can compile code for UNIX on windows with minimum distress

### Download files

```bash
$ wget https://pbfcomics.com/wp-content/uploads/2019/10/PBF-The_Talk.png
--2019-10-14 09:17:59--  https://pbfcomics.com/wp-content/uploads/2019/10/PBF-The_Talk.png
Resolving pbfcomics.com (pbfcomics.com)... 162.243.240.67
Connecting to pbfcomics.com (pbfcomics.com)|162.243.240.67|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 246081 (240K) [image/png]
Saving to: ‘PBF-The_Talk.png’

PBF-The_Talk.png    100%[===================>] 240.31K   552KB/s    in 0.4s    

2019-10-14 09:18:00 (552 KB/s) - ‘PBF-The_Talk.png’ saved [246081/246081]
$ eog PBF-The_Talk.png
```

### Push Pop

```bash
$ pushd .
~/Documents/Praga/Teaching/Tools_DigitalAge ~/Documents/Praga/Teaching/Tools_DigitalAge
$ popd
~/Documents/Praga/Teaching/Tools_DigitalAge
```

![poppush](/home/fex/Documents/Praga/Lezioni/Clean/im_l2/Lifo_stack.png)

[^]: Image taken from https://commons.wikimedia.org/wiki/File:Lifo_stack.png . This file is made available under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons)[CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en)

pushd added a directory to the folder stack. 'pushd .' add the currently directory (remembers the folder!) popd gets you to the first folder in the stack. useful for script, no need of writing complex addresses to check the stack:

```bash
$ dirs
~/Documents/Praga/Teaching/Tools_DigitalAge
```

###  Pro Tips

If you put this char after a command 

- `;` chain up commands on the same line
- `&&` chain-up commands only if the previous command do **not** fail

- `&` run the command, let it run, and move on, aka it sends it in the background
- to check what is running in the background `ps` or `jobs`
  - `ps` lists all the  processes including bash and `ps` itself
  - `jobs` only the commands sent with the terminal

```bash
$ pwd; ls
/home/fex/Documents/Praga/Teaching/Tools_DigitalAge/lesson2
file.jpg  Linux-Commands-for-Network.jpg  one.dat  two.dat
$ pwd; ls; cp onee.dat three.dat && pwd 
/home/fex/Documents/Praga/Teaching/Tools_DigitalAge/lesson2
file.jpg  Linux-Commands-for-Network.jpg  one.dat  two.dat
cp: cannot stat 'onee.dat': No such file or directory
$ pwd; ls; cp onee.dat three.dat ;pwd 
/home/fex/Documents/Praga/Teaching/Tools_DigitalAge/lesson2
file.jpg  Linux-Commands-for-Network.jpg  one.dat  two.dat
cp: cannot stat 'onee.dat': No such file or directory
/home/fex/Documents/Praga/Teaching/Tools_DigitalAge/lesson2
$ echo {1..10} & pwd
[1] 17830
/home/fex/Documents/Praga/Teaching/Tools_DigitalAge/lesson2
1 2 3 4 5 6 7 8 9 10
```

More tips and tricks: [here](https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je)

# Link
A link is a virtual file that is somehow a copy of another one, or a shortcut for it.

Linking by default is HARD: `ln`
SOFT links are given by: `ln -s`
Link file:

- similar to windows Shortcuts. In windows are used to launch software from desktop
- In Linux 2 types: soft and hard.
     ### Soft Link
     Also called symbolic. It is the same as the  WMS10 shortcut. It is a new file, that points to another files. 
     It's a file that contains the PATH of the original file.
     
     ### Hard Link
     - Not doable for directories
     - Same portion of the disk, same rights etc
     - The same physical data on a disk is connected to different files.
       -   If the original data are changed, all the linked files are changed.
       - If one is cancelled only that one is cancelled
       - They take up less space on disk
     

```bash
$ ls -lia
total 96
5251075 drwxr-xr-x 2 fex fex  4096 Oct 11 18:39 .
5246853 drwxr-xr-x 5 fex fex  4096 Oct 14 14:40 ..
5247102 -rw-r--r-- 1 fex fex 37791 Oct 11 18:38 file.jpg
5247149 -rw-r--r-- 1 fex fex 37791 Oct 11 15:35 Linux-Commands-for-Network.jpg
5247146 -rw-r--r-- 1 fex fex    19 Oct 11 18:14 one.dat
5247144 -rw-r--r-- 1 fex fex    10 Oct 11 18:14 two.dat
$ ln -s one.dat soft_link_of_one.dat 
$ ln one.dat HARD_one.dat
$ ls -lia
total 100
5251075 drwxr-xr-x 2 fex fex  4096 Oct 14 14:42 .
5246853 drwxr-xr-x 5 fex fex  4096 Oct 14 14:40 ..
5247102 -rw-r--r-- 1 fex fex 37791 Oct 11 18:38 file.jpg
5247146 -rw-r--r-- 2 fex fex    19 Oct 11 18:14 HARD_one.dat
5247149 -rw-r--r-- 1 fex fex 37791 Oct 11 15:35 Linux-Commands-for-Network.jpg
5247146 -rw-r--r-- 2 fex fex    19 Oct 11 18:14 one.dat
5244385 lrwxrwxrwx 1 fex fex     7 Oct 14 14:42 soft_link_of_one.dat -> one.dat
5247144 -rw-r--r-- 1 fex fex    10 Oct 11 18:14 two.dat
```

[Notes for Digital-Age tools for researcher ](https://gitlab.com/cippo1987/DigitalageTools) © 2021   by  [ Federico Brivio ](https://gitlab.com/cippo1987/) is licensed under [ CC BY-NC 4.0 ![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxxJREFUSIm11luIVlUUB/DfzKhTwQQW2nQhiHwJAiOYyKjEKGhSoyCyh4QIulHQjSBf6sGp6AKVURBERUlJBBH1EPSQhVA9dBG6aPegtBG6TdNUjs7Xw97Hs77t+Zzv0/EPh3PO3v+9/nuvtfdau093OBIrsBpLcRyG0cIvGMeneBOb8U+XdjtiGI9jMot080ziCZxwsKJr8UeD4b/wJd6VVvcFJhp4E7iuF8E+3F8Y+ROP4HzMaxgzgHPxcMNkn8L8bkRfDINm8CQW9TDxY/Eo9gY7b80mfm8g/4urexAsMap99c92Il4mrbCF/7D8EEQrnKE9/teXhCPwYyD0tClmwSj2ZLu/Y2HsvD2IvjSHohU2BPtjVeOAlAQqF59yGIQXq10+gcF5WCZlItiI74tBi3ANTscUPsImKVFUOBNrpKSxS9rFb4f+XdLmuhVD8v55SO2G0UJ0GX61f3L4BksyZ516U8ZnE/qDrRWh7zFSBmrhbwwG4lHYoU6D9+Au/JyF1uPCYGwrbpLiuUdy6dnB3nx16t0CX+Wf7cVqrwpGbwvtSzGSv1/J/dM4KXAu1Zx0tmf+16Tc28I7BWl9EB7RjM/Vru8GmzN/MsagryC1wne/ZrRm6S9Racz0Y2f+Ob4gRddfEL5PxLVSsdiW207GqYGzSnt8KyzO73F4L898SspgFYakY1Dl7THcgM9y29NYqQ7HNtws5fup3HZRsDegPstbSKWsGryymOFyKc2VR+UD9ea5r6F/RiqtEeeE/g1wXmh4roN77sTzUolcY/8SN4IH8AIelBJKiVjjL65cMJ4bdqsTw1xiIX5T32D2hfSOMJtXD4NwDGdbCBbg29B5yxyKlmXxmJKwSn1dmc7/h4qztF8EbuxEXBdI01I1OVisle7Xlb2NByL34RntR+N1nNaD4BKpMkUbr0nhnBV3a78lTuNlXImjG/hDuEJa1W7t53lM9ykVXILvipm38oR24hN8LJXOvQ28H3B5L4IRC6SSWF2Nunl25DGDDfb2oaxInTAg3UZWS8l/WCoWLfyUJ/Y+3sCHkosPiP8Btp0swL9OhkEAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAArlJREFUSIm11k9oHVUUBvBfX2Ka11TbGkhaWwPu3FmoQiBBsXShNEEEC9KFkIVIN64sKAUXhVaolJZIIkZB6ap0kYUKpSstrbpw4UJqu1RjSlOMljTEpK2NizvDu5k3M5kXkw8uM9z7nfPdc/+cezaphjr2YwjPoBc7sYxbmMENfIMvMVfRbyG24wPMJyJV2gLGsWetoiOYzXE8j+u4jG/xixBhHu9IK4JtOJNxMofTeAHtBTaDOIW/M7af4JHVRGuYjIweCsvW08LEu5OJ/xv5ubSa+KmI/A9eL+A9i8+Ttq+AM2zlFnxcJHo4Ii0Ky5qHTbgZcaeTvjzszYi/lSVsFa5FShgpmh0e03yQHi3hD2ss+xx2x4PvR07OlThJcS3i/1yBHx/Wj9LODtzRuINPVnDUg7eTVuXgdUca86i3C3u5LSF8gakSB8fxdKZvMPn+JCSbPMziU7yDLrwIoxrLcKBEtK55b7OtXmL/fMQbg6saS9BRYthVQbirxL4ddxPedzXsSgamcK/E8P/igXD1oLcmvDSE67TRSDV21oTQKU4C64lUY7kmvKWE93Wjka7uTE0j/D3KE/mSkPImcsYmkrGlEvs2PJH834azql2nFIc0n+ZDFewGIv54DV9Hg69WcLBWDEf/Fwl39y+Np7BvFQdrifjxSGMBW2rC3f0wIXTiZNUQWsAx7Ej+xxJxhFT3u0YEZbVSqxEfFJLHslAWdWcJr2i8m/fw8joID1hZob5ZNLtjEem+/Mj7cSHT+nN4bwhnJvU3WiRKyCyfZaI5j6fKjDLoE4qJ2Mek/Oq0SfxdK6vEReE9PSj/6duMl4RSNo7yIU4I1WtlDOE3zfu5mPT/gO/xa0YsbX/gtVYEY3TiKP7McVzUZvEetpQ5rvoitQsVxBCeE5J9mnenhdz7I77CFeH6lOI/baALunnzplQAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxRJREFUSIml1luoZ3MUB/DPueaMQ0PkKLc8eOWBooTIA2eGkii8jHKb4kGMePCiyWAkU2Z4QEnydMp40ESMa4ohI3XSKJcZuR3jcsxw5jh/D2vv8//tfX577/+MVb/6//de67vWXr/vugwZTCZwKdbgbJyEKfTwA37ELN7CdvwxIG6jrMbDmC+cDHIOYCtOOVKn6zCXAZ7DbuzAa/gMP2f05nHH4TgcwRM1kN+wCedjOGMzhPPwEH6p2T6DsS6nw5hJjJbwJE44jMBX4xEsJjg7upw/migfxHUtulM4Uz4DcCV+T/CebgK6IVH6Gxe1RYhXCt3JFp1zBMNL3BV3PinKolRY1+F0UMcwrZ/2P9XY/mDi9Fdcj9EMyDCuwcuFXg/v4HFR302yOcF/qnw4LlhbL4evcTeOLfRGCodN9buEexocH58E+pdoSC5vAesJgmzGhtrzb/BuAvg5Tmz56k2J7TRsqT3YgO86gnk+ARzHraKNtsmFif1WeE//4staGxMs/7jB8SyuFekfVEYKHz28D3uKP7sbDC4Rjf/fTABf4S7dzC5ltrDbI4nizQ6js0SKcgNjv2g+p3Zg7NTP7rLjnQNGfRzuw95MAAt4Cec22L6uP0CWUz07oONSxnETPs0E0MPbuFq1pe7Sv6Jlcs3rniI5Mh0jf//l+RLrBQ/K7H5AdQRe1uJ0leDBNtV6vSCxX8Jz8jxIB8Y2YqVZ0c5qMoE3Er1DIsUfFr/T8Uc7D3pYS6S33DQO4rSM41FsxD8NQD18YWUTGcON+CTRO1BkD9V2+GLDV8PpeEBMpnLTmMFtgmxNkg6Jx9IXE/g2eXl7C0gp2wvdozv0rtAfi/vFwKjIGn12LhQG/9fxxapEu6VJMU35IdzZAjopSDTU8P5mVU5sacFCUD0lzYxol4PKGaJ71TFyi8UKuVe1KSzgBbF95AbCKlyFZ8W+ltb1Rs0LYVamxbCvl80ivhd1vAv7VFfZ8uwVo/OI5Chx7/Ulve3M4X5JreakiRh1GRXr7loxeaZwcvFuH37CR3hVrEOLXYD/AYTjaOgDscSLAAAAAElFTkSuQmCC)](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)

Your rights are available [here](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1).
