[[_TOC_]]



# WYSIWYG vs WYSIWYM

   **WYSIWYG** = What you see is what you get. FORMATTED TEXT

 - wy*See*iwyg. you WORK GRAPHICALLY on your text. 
 - Text should be about CONTENT, Relationship between words, etc. NOT about graphical make up
 - The rendering of figures happens live
 - Word, libreOffice, notepad
 - Need of extra software, issue of compatibility etc
   

  **WYSIWYM** = What You See Is What You Mean. PLAIN TEXT
   - You have no control on graphic appearance when you write. You are writing down relationship, format, etc in a non-human language
   - BIG ISSUE you need to think in a different way
   - Often IS VERY DIFFICULT/IMPOSSIBLE to get what you want
          - Most of the time if you can not get it, it's better that way
   - Different behaviour of Word or similar. 
       - Steep learning curve
       - Decreasing difficulty (respect Word) as the text increases in lenght.
   - The transformation from code to graphical text, happens AFTER the writing
     
# WYSIWYM

 There are several code of this type.
 Most used is probably HTML code and all the derived.
 In principle, analogous to compiled code: set of commands are transformed in a text, rather than an executable binary.
 For this purpose we consider two cases.


## MarkDown

 This is the language used for this text.
 - Extremely Easy
 - Line based
 - Extremely compatible
 - Great for notes, or short text
 - Easy to read even if not compiled
 - Good looking
 - Several online services: [dillinger.io](dillinger.io,), [stackedit](stackedit.io) or local software *typora* 

 **QT - 1** Can a *.doc* be converted in *.MD*? How?

### Specifications
  The specifications are easy to get checking any (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)[CheatSheet] 
  - Be careful there are different flavours (different commands to insert figures, or some language with extra powers, such as shortcut)
    Basics:
  - \# \## before a line makes a word a title. The title are ranked from 1 \# to N \#  where the lower the number of hash, the more important is the title.
        - The max number of hashes depends on the flavour of mark down, usually you are safe up to 4
  - \* \*\* are used as emphases \*TEST\* = *TEST*  \*\*TEST\*\*=**TEST**
  - \- or \.N are used to create list or numbered list
  - If you want to put an hyperlink you need to put linked word in squared brackets follow it with the URL in round one ()
        - \[linked\]\(www.whatever.we\) appears as [linked](www.whatever.we)
  - Figures are similar, you need to put a \! before the square brackets.  Check the particular flavour

  **QT - 2**   

1. What type of file should a MD text be?

  2. GitHub (and GitLab) exploit MD for the README files, and Manual. Why?
  3. What other website has a similar format? Why?

  **THM**: Introduction of flavours, links, escape char, etc



### Math

One of the best feature of some MD code is that you can insert Math equation written in $\LaTex$ . Beware this depends a lot on the software you are using and you do not have full compatibility. In general the tool used for it is [MathJax](https://www.mathjax.org/)

If you are using Typora a full guide is [here](https://support.typora.io/Math/).

This is one random example. 
$$
\sum^{\infin}_{i=42}  \int \int_a^b \frac{\Phi(x,t)}{\hat{s}} dx
$$
This equation is created putting between two pair of \$ the LaTex equation:

 \$\$ \\sum\^\{\\infin\}\_\{i=42\}  \\int \\int\_a\^b \\frac\{\\Phi\(x,t\)\}\{\\hat\{s\}\} dx \$\$

## Latex
Latex is more complex then MD. On the other hand can be used to create more complex texts. 
With respect to M$Word:

- **FORCE YOU TO THINK ABOUT THE STRUCTURE**
- File are lighter (easy to transfer)
- Once compiled is an universal PDF (or other format)
- Difficult to learn, but quite intuitive to edit any template
- Better handling of large files (thesis)
- Faster to process images, calculations, formulas.

# Latex bits
To make easier to understand how Latex Works it is still useful to give a little overview for scientific purposes. 

## Tools
Formaly LaTex is just a plain ASCII TEXT which contains code.
More information is found on the [Official Website](https://www.latex-project.org/)

### The compiler
The first component we need is the **compiler**, called in analogy with programming-languages compilers:
a software that transform your SOURCE CODE `.tex` file into a file with formatted text.
In general the comand

`latex file.tex` 

creates `file.dvi` DVI (DeVice Independent) which is the standard output. It is a binary file!
also 
> DVI differs from PostScript and PDF in that it does not support any form of font embedding. 

This is not very useful!

PDFlatex creates a PDF, other option allows to reate RTF files or PostScript files. 

The command can be run through any terminal --> BASH

### The Editor

Any TEXT editor can create `.tex` files!
The best choice is to use LaTeX editor, such as MikTex or TeXmaker on Linux.
- Shortcut for more complex commands
- Handling of packages (see later)
- Compilation of the code without need of using a terminal
- GUI
- Autocompletion
- Spelling Check (lack of Grammar check in the basic version though)
etc etc.

## Structure

Latex is mostly divided in two parts: header and body.

### Header

The header contains several data about your document.
The class (=type) of document, the paper format (A4, B5, landscape etc), the language, the packaged (See later), the Metadata (author, date, etc).

#### Packages

LaTex is modular. Rather than a complex code that does *everything*, it is a minimal code that does one thing:
take a text, convert it in a nice format.
Format-based programs on the other hand **allow** several things: fonts, size, shades, weird format, etc etc. Despite that they somehow **allow** you to do it! You can do ONLY things they implemented.
LaTex is minimal, it has few things, but these are modular:
you (or most likely someone on the internet) can write NEW FUNCTIONS that do specific things:
- Music sheets
- Chemical formula
- Mathematical formula
- Diagram
- Feynmann Diagrams
- Weird packags as coffea stains

### Document Body
The document body contains your text, it has access to the header data, so for example you can use specific comands to retrieve metadata from the header.

The Body does not contains much informations about the formatting of the document (that's in the header),but you need to keep format in mind.
For example, the size of the figures has to be sensible with respect to the size of the paper. 
There is no way to know it from the size without knowing the details of figures and page.

#### Reference
Bibliography references can be done using two different packages:
- natbib
- bibtex
Read carefully what you are using since some rules are different. Consider Importing `.bib` files from google scholar. Super easy.


REFs need to be compiled too.
So the proper way to copile a doc with reference in latex is: LaTex, BibTeX, LaTex, LaTex. The first compilation with LaTex compiles the text and leaves holes for the reference, then reference are compiled, the blank holes are filled, and the last compilation reorder them in the proper order. 
This chain of compilation is usually automatically done by quick-compile, or PDFLaTEx if you use an editor code.

Be aware the latex create several aux file during the compilation, that's why you need several steps, but nowadays most of this machinery is hidden 

#### Presentation
There is one famous special class of documents: BEAMER. This is used to make talks. The defaults are pretty ugly but with minimal tweak you can get great presentations.

## HOW TO START
### MD
MarkDown is super easy I reccomend:
- subl
- stakedit
- dillinger

### LaTex
Remember you need to install both the language compiler and a good editor:
`sudo apt-get install texlive texmaker`
To learn there are two main ways 
1. Using `LyX`: LyX is a WYSIWYM editor for Latex, is somehow an hybrid version of `LaTex` and `MarkDown` editors. It was the first version of LaTex I used during my master thesis. I learned it in a reasonable amount of time. I think nowadays is not recommended, but it could be fun to have a look at it.
2. Overleaf. It's an online editor
    - Several template already written. Easy to learn from
    - No issue with compilations
    - just change the text


In general on `LaTex` any easy task is ... *easy* the issue is that sometime for more complex task you might found several ways, and use several packages that often go in conflict. Only experience can help you, but nowadays there is a huge community out there (i.e. google) 


#### XeTex
There are several different alternative to `LaTex` (one i mentioned is `pdflatex`), one interesting one is XeTex, which is often used as default by many compiler services because it has unicode font support (copy-paste)

**QT** What is unicode?

[Notes for Digital-Age tools for researcher ](https://gitlab.com/cippo1987/DigitalageTools) © 2021   by  [ Federico Brivio ](https://gitlab.com/cippo1987/) is licensed under [ CC BY-NC 4.0 ![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxxJREFUSIm11luIVlUUB/DfzKhTwQQW2nQhiHwJAiOYyKjEKGhSoyCyh4QIulHQjSBf6sGp6AKVURBERUlJBBH1EPSQhVA9dBG6aPegtBG6TdNUjs7Xw97Hs77t+Zzv0/EPh3PO3v+9/nuvtfdau093OBIrsBpLcRyG0cIvGMeneBOb8U+XdjtiGI9jMot080ziCZxwsKJr8UeD4b/wJd6VVvcFJhp4E7iuF8E+3F8Y+ROP4HzMaxgzgHPxcMNkn8L8bkRfDINm8CQW9TDxY/Eo9gY7b80mfm8g/4urexAsMap99c92Il4mrbCF/7D8EEQrnKE9/teXhCPwYyD0tClmwSj2ZLu/Y2HsvD2IvjSHohU2BPtjVeOAlAQqF59yGIQXq10+gcF5WCZlItiI74tBi3ANTscUPsImKVFUOBNrpKSxS9rFb4f+XdLmuhVD8v55SO2G0UJ0GX61f3L4BksyZ516U8ZnE/qDrRWh7zFSBmrhbwwG4lHYoU6D9+Au/JyF1uPCYGwrbpLiuUdy6dnB3nx16t0CX+Wf7cVqrwpGbwvtSzGSv1/J/dM4KXAu1Zx0tmf+16Tc28I7BWl9EB7RjM/Vru8GmzN/MsagryC1wne/ZrRm6S9Racz0Y2f+Ob4gRddfEL5PxLVSsdiW207GqYGzSnt8KyzO73F4L898SspgFYakY1Dl7THcgM9y29NYqQ7HNtws5fup3HZRsDegPstbSKWsGryymOFyKc2VR+UD9ea5r6F/RiqtEeeE/g1wXmh4roN77sTzUolcY/8SN4IH8AIelBJKiVjjL65cMJ4bdqsTw1xiIX5T32D2hfSOMJtXD4NwDGdbCBbg29B5yxyKlmXxmJKwSn1dmc7/h4qztF8EbuxEXBdI01I1OVisle7Xlb2NByL34RntR+N1nNaD4BKpMkUbr0nhnBV3a78lTuNlXImjG/hDuEJa1W7t53lM9ykVXILvipm38oR24hN8LJXOvQ28H3B5L4IRC6SSWF2Nunl25DGDDfb2oaxInTAg3UZWS8l/WCoWLfyUJ/Y+3sCHkosPiP8Btp0swL9OhkEAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAArlJREFUSIm11k9oHVUUBvBfX2Ka11TbGkhaWwPu3FmoQiBBsXShNEEEC9KFkIVIN64sKAUXhVaolJZIIkZB6ap0kYUKpSstrbpw4UJqu1RjSlOMljTEpK2NizvDu5k3M5kXkw8uM9z7nfPdc/+cezaphjr2YwjPoBc7sYxbmMENfIMvMVfRbyG24wPMJyJV2gLGsWetoiOYzXE8j+u4jG/xixBhHu9IK4JtOJNxMofTeAHtBTaDOIW/M7af4JHVRGuYjIweCsvW08LEu5OJ/xv5ubSa+KmI/A9eL+A9i8+Ttq+AM2zlFnxcJHo4Ii0Ky5qHTbgZcaeTvjzszYi/lSVsFa5FShgpmh0e03yQHi3hD2ss+xx2x4PvR07OlThJcS3i/1yBHx/Wj9LODtzRuINPVnDUg7eTVuXgdUca86i3C3u5LSF8gakSB8fxdKZvMPn+JCSbPMziU7yDLrwIoxrLcKBEtK55b7OtXmL/fMQbg6saS9BRYthVQbirxL4ddxPedzXsSgamcK/E8P/igXD1oLcmvDSE67TRSDV21oTQKU4C64lUY7kmvKWE93Wjka7uTE0j/D3KE/mSkPImcsYmkrGlEvs2PJH834azql2nFIc0n+ZDFewGIv54DV9Hg69WcLBWDEf/Fwl39y+Np7BvFQdrifjxSGMBW2rC3f0wIXTiZNUQWsAx7Ej+xxJxhFT3u0YEZbVSqxEfFJLHslAWdWcJr2i8m/fw8joID1hZob5ZNLtjEem+/Mj7cSHT+nN4bwhnJvU3WiRKyCyfZaI5j6fKjDLoE4qJ2Mek/Oq0SfxdK6vEReE9PSj/6duMl4RSNo7yIU4I1WtlDOE3zfu5mPT/gO/xa0YsbX/gtVYEY3TiKP7McVzUZvEetpQ5rvoitQsVxBCeE5J9mnenhdz7I77CFeH6lOI/baALunnzplQAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxRJREFUSIml1luoZ3MUB/DPueaMQ0PkKLc8eOWBooTIA2eGkii8jHKb4kGMePCiyWAkU2Z4QEnydMp40ESMa4ohI3XSKJcZuR3jcsxw5jh/D2vv8//tfX577/+MVb/6//de67vWXr/vugwZTCZwKdbgbJyEKfTwA37ELN7CdvwxIG6jrMbDmC+cDHIOYCtOOVKn6zCXAZ7DbuzAa/gMP2f05nHH4TgcwRM1kN+wCedjOGMzhPPwEH6p2T6DsS6nw5hJjJbwJE44jMBX4xEsJjg7upw/migfxHUtulM4Uz4DcCV+T/CebgK6IVH6Gxe1RYhXCt3JFp1zBMNL3BV3PinKolRY1+F0UMcwrZ/2P9XY/mDi9Fdcj9EMyDCuwcuFXg/v4HFR302yOcF/qnw4LlhbL4evcTeOLfRGCodN9buEexocH58E+pdoSC5vAesJgmzGhtrzb/BuAvg5Tmz56k2J7TRsqT3YgO86gnk+ARzHraKNtsmFif1WeE//4staGxMs/7jB8SyuFekfVEYKHz28D3uKP7sbDC4Rjf/fTABf4S7dzC5ltrDbI4nizQ6js0SKcgNjv2g+p3Zg7NTP7rLjnQNGfRzuw95MAAt4Cec22L6uP0CWUz07oONSxnETPs0E0MPbuFq1pe7Sv6Jlcs3rniI5Mh0jf//l+RLrBQ/K7H5AdQRe1uJ0leDBNtV6vSCxX8Jz8jxIB8Y2YqVZ0c5qMoE3Er1DIsUfFr/T8Uc7D3pYS6S33DQO4rSM41FsxD8NQD18YWUTGcON+CTRO1BkD9V2+GLDV8PpeEBMpnLTmMFtgmxNkg6Jx9IXE/g2eXl7C0gp2wvdozv0rtAfi/vFwKjIGn12LhQG/9fxxapEu6VJMU35IdzZAjopSDTU8P5mVU5sacFCUD0lzYxol4PKGaJ71TFyi8UKuVe1KSzgBbF95AbCKlyFZ8W+ltb1Rs0LYVamxbCvl80ivhd1vAv7VFfZ8uwVo/OI5Chx7/Ulve3M4X5JreakiRh1GRXr7loxeaZwcvFuH37CR3hVrEOLXYD/AYTjaOgDscSLAAAAAElFTkSuQmCC)](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)

Your rights are available [here](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1).
