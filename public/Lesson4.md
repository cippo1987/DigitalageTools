[[_TOC_]]

# GIT
Is the most popular software to manage *versioning*
![git](/home/fex/Documents/Praga/Lezioni/Clean/im_l4/220px-VersionNumbers.svg.png)

[^]: Taken from https://commons.wikimedia.org/wiki/File:VersionNumbers.svg . This file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons) [Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.

Code versioning is a way to keep track of code evolution and different authors contributions

GIT (developed by Linus Torvalds) has several functions. We need to understand few of them before starting. 


 ## Structure

![git2](/home/fex/Documents/Praga/Lezioni/Clean/im_l4/Git_operations.svg.png)

[^]: Taken from https://en.wikipedia.org/wiki/File:Git_operations.svg . This file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons)[Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.




 1. **REMOTE** This is a server which keeps tracks of all the modifications and allows people to exchange contributions


 2. **CLONE** A *local* copy of the server. 

 3. **BRANCHES** Line of development of a code. Different authors for example, or different scopes. 

 4. **Working Files** The group of files open and that are edited.

 5. **STAGE** Before sending modifications back to the server files are added in Stage. There it is possible to modify the files before uploading, or checking if everything is ok. It is a sort of reversed recycled bin

 ## Commands

 - *pull* Pulling files from remote repository. If the whole rep is copied then it is a `cloning`
 - *push* uploading files back to the remote
 - *add* move a working file to the stage.
 - *commit* add the stage file to the branch

# GITLAB/GITHUB
GIT is a software to handle versioning. You need to create a DB/server for your data. Or you need to share the information. 
Alternative is to have a service that handle that for you.
[GITLAB](www.gitlab.com)/[GITHUB](www.github.com)are online services that they interfaced with local version of GIT, they allow the management of data storage, access, transfer, users, versioning, releases, etc etc

***

We select **GITLAB** for several reasons: easy, common, hosting services, etc etc

# GITLAB: primer
1. Gitlab uses repositories as much as github but also allows the creation of projects, folders for repositories.
2. We have a group gitlab. A shared profile for common, sharable documents
3. Many operations, and small edits can be done online with WEB-GUI. But it is better to work remotely
4. Everything **TEXT-BASED** can be hosted as a repository

***
## Working on command line
A simple guide is also available [here](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

```bash
$ git --version

Command 'git' not found, but can be installed with:

sudo apt install git
$ sudo apt-get -y install git
$ git --version
git version 2.20.1
```

Configuration

```bash
$ git config --global --list
fatal: unable to read config file '/home/fex/.gitconfig': No such file or directory

Need to set own username
$ git config --global user.name "Putyournickhere"
$ git config --global user.name
Putyournickhere
$ git config --global user.email "mail@mail.com"
$ git config --global --list
user.name=Putyournickhere
user.email=mail@mail.com
```

Start to use it

```bash
$ cd $PATHOFCHOICE
$ git init
Initialised empty Git repository in $PATHOFCHOICE/.git/
```

This created an hidden folder (name starts with .) called `.git` which keeps the information for the repository.

if you want to clone a repo available online on gitlab

```bash
$ git clone git@gitlab.com:user/lesson_4.git
Cloning into 'lesson_4'...
The authenticity of host 'gitlab.com (35.231.145.151)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
```

**QT** Do you need to `init` if you are cloning?

[Notes for Digital-Age tools for researcher ](https://gitlab.com/cippo1987/DigitalageTools) © 2021   by  [ Federico Brivio ](https://gitlab.com/cippo1987/) is licensed under [ CC BY-NC 4.0 ![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxxJREFUSIm11luIVlUUB/DfzKhTwQQW2nQhiHwJAiOYyKjEKGhSoyCyh4QIulHQjSBf6sGp6AKVURBERUlJBBH1EPSQhVA9dBG6aPegtBG6TdNUjs7Xw97Hs77t+Zzv0/EPh3PO3v+9/nuvtfdau093OBIrsBpLcRyG0cIvGMeneBOb8U+XdjtiGI9jMot080ziCZxwsKJr8UeD4b/wJd6VVvcFJhp4E7iuF8E+3F8Y+ROP4HzMaxgzgHPxcMNkn8L8bkRfDINm8CQW9TDxY/Eo9gY7b80mfm8g/4urexAsMap99c92Il4mrbCF/7D8EEQrnKE9/teXhCPwYyD0tClmwSj2ZLu/Y2HsvD2IvjSHohU2BPtjVeOAlAQqF59yGIQXq10+gcF5WCZlItiI74tBi3ANTscUPsImKVFUOBNrpKSxS9rFb4f+XdLmuhVD8v55SO2G0UJ0GX61f3L4BksyZ516U8ZnE/qDrRWh7zFSBmrhbwwG4lHYoU6D9+Au/JyF1uPCYGwrbpLiuUdy6dnB3nx16t0CX+Wf7cVqrwpGbwvtSzGSv1/J/dM4KXAu1Zx0tmf+16Tc28I7BWl9EB7RjM/Vru8GmzN/MsagryC1wne/ZrRm6S9Racz0Y2f+Ob4gRddfEL5PxLVSsdiW207GqYGzSnt8KyzO73F4L898SspgFYakY1Dl7THcgM9y29NYqQ7HNtws5fup3HZRsDegPstbSKWsGryymOFyKc2VR+UD9ea5r6F/RiqtEeeE/g1wXmh4roN77sTzUolcY/8SN4IH8AIelBJKiVjjL65cMJ4bdqsTw1xiIX5T32D2hfSOMJtXD4NwDGdbCBbg29B5yxyKlmXxmJKwSn1dmc7/h4qztF8EbuxEXBdI01I1OVisle7Xlb2NByL34RntR+N1nNaD4BKpMkUbr0nhnBV3a78lTuNlXImjG/hDuEJa1W7t53lM9ykVXILvipm38oR24hN8LJXOvQ28H3B5L4IRC6SSWF2Nunl25DGDDfb2oaxInTAg3UZWS8l/WCoWLfyUJ/Y+3sCHkosPiP8Btp0swL9OhkEAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAArlJREFUSIm11k9oHVUUBvBfX2Ka11TbGkhaWwPu3FmoQiBBsXShNEEEC9KFkIVIN64sKAUXhVaolJZIIkZB6ap0kYUKpSstrbpw4UJqu1RjSlOMljTEpK2NizvDu5k3M5kXkw8uM9z7nfPdc/+cezaphjr2YwjPoBc7sYxbmMENfIMvMVfRbyG24wPMJyJV2gLGsWetoiOYzXE8j+u4jG/xixBhHu9IK4JtOJNxMofTeAHtBTaDOIW/M7af4JHVRGuYjIweCsvW08LEu5OJ/xv5ubSa+KmI/A9eL+A9i8+Ttq+AM2zlFnxcJHo4Ii0Ky5qHTbgZcaeTvjzszYi/lSVsFa5FShgpmh0e03yQHi3hD2ss+xx2x4PvR07OlThJcS3i/1yBHx/Wj9LODtzRuINPVnDUg7eTVuXgdUca86i3C3u5LSF8gakSB8fxdKZvMPn+JCSbPMziU7yDLrwIoxrLcKBEtK55b7OtXmL/fMQbg6saS9BRYthVQbirxL4ddxPedzXsSgamcK/E8P/igXD1oLcmvDSE67TRSDV21oTQKU4C64lUY7kmvKWE93Wjka7uTE0j/D3KE/mSkPImcsYmkrGlEvs2PJH834azql2nFIc0n+ZDFewGIv54DV9Hg69WcLBWDEf/Fwl39y+Np7BvFQdrifjxSGMBW2rC3f0wIXTiZNUQWsAx7Ej+xxJxhFT3u0YEZbVSqxEfFJLHslAWdWcJr2i8m/fw8joID1hZob5ZNLtjEem+/Mj7cSHT+nN4bwhnJvU3WiRKyCyfZaI5j6fKjDLoE4qJ2Mek/Oq0SfxdK6vEReE9PSj/6duMl4RSNo7yIU4I1WtlDOE3zfu5mPT/gO/xa0YsbX/gtVYEY3TiKP7McVzUZvEetpQ5rvoitQsVxBCeE5J9mnenhdz7I77CFeH6lOI/baALunnzplQAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxRJREFUSIml1luoZ3MUB/DPueaMQ0PkKLc8eOWBooTIA2eGkii8jHKb4kGMePCiyWAkU2Z4QEnydMp40ESMa4ohI3XSKJcZuR3jcsxw5jh/D2vv8//tfX577/+MVb/6//de67vWXr/vugwZTCZwKdbgbJyEKfTwA37ELN7CdvwxIG6jrMbDmC+cDHIOYCtOOVKn6zCXAZ7DbuzAa/gMP2f05nHH4TgcwRM1kN+wCedjOGMzhPPwEH6p2T6DsS6nw5hJjJbwJE44jMBX4xEsJjg7upw/migfxHUtulM4Uz4DcCV+T/CebgK6IVH6Gxe1RYhXCt3JFp1zBMNL3BV3PinKolRY1+F0UMcwrZ/2P9XY/mDi9Fdcj9EMyDCuwcuFXg/v4HFR302yOcF/qnw4LlhbL4evcTeOLfRGCodN9buEexocH58E+pdoSC5vAesJgmzGhtrzb/BuAvg5Tmz56k2J7TRsqT3YgO86gnk+ARzHraKNtsmFif1WeE//4staGxMs/7jB8SyuFekfVEYKHz28D3uKP7sbDC4Rjf/fTABf4S7dzC5ltrDbI4nizQ6js0SKcgNjv2g+p3Zg7NTP7rLjnQNGfRzuw95MAAt4Cec22L6uP0CWUz07oONSxnETPs0E0MPbuFq1pe7Sv6Jlcs3rniI5Mh0jf//l+RLrBQ/K7H5AdQRe1uJ0leDBNtV6vSCxX8Jz8jxIB8Y2YqVZ0c5qMoE3Er1DIsUfFr/T8Uc7D3pYS6S33DQO4rSM41FsxD8NQD18YWUTGcON+CTRO1BkD9V2+GLDV8PpeEBMpnLTmMFtgmxNkg6Jx9IXE/g2eXl7C0gp2wvdozv0rtAfi/vFwKjIGn12LhQG/9fxxapEu6VJMU35IdzZAjopSDTU8P5mVU5sacFCUD0lzYxol4PKGaJ71TFyi8UKuVe1KSzgBbF95AbCKlyFZ8W+ltb1Rs0LYVamxbCvl80ivhd1vAv7VFfZ8uwVo/OI5Chx7/Ulve3M4X5JreakiRh1GRXr7loxeaZwcvFuH37CR3hVrEOLXYD/AYTjaOgDscSLAAAAAElFTkSuQmCC)](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)

Your rights are available [here](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1).
