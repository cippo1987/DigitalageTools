[[_TOC_]]



# Social Media

1. Science is based on social interactions
2. With the increase of size of science it is more difficult to emerge and keep track
3. Impact Factor (IF), dates from 1975, Eugene Garfield, but then  evolved in several metrics.
   - Kardashian index: https://www.sciencemag.org/news/2014/09/top-50-science-stars-twitter#full-list
   - Altmetric Attention Score (News    8, Blog    5 Policy document (per source)    3, Patent    3, Wikipedia    3,  Twitter (tweets and retweets)    1, ... )
   - https://pubpeer.com/ PubPeer a site to leave comments on papers
   - Clarivate Analytics

This set of *services* has changed the shape of science.

# Open Access Journals

Open Access (OA) papers can be read by everyone without a subscription. 

The logic behind this is that most research is performed with public money, and reviewed for free by the community. So the whole population should have free access to it. Some journal publish only Open Access. Other allow the author to pay to make their paper open access.

In general OA is SOLD and PAID with PUBLIC MONEY. 

Thankfully the EU has recently started to impose OA to all the work they publish. Most information are available [here](https://ec.europa.eu/info/research-and-innovation/strategy/goals-research-and-innovation-policy/open-science/open-access_en).

To check the policy of a specific journal and what papers they allow it is convenient to check  the [Sharpa/RoMEO](https://www.sherpa.ac.uk/romeo/browse.php?colour=green ) code 

> ```
> RoMEO is a searchable database of publisher's policies regarding the self- archiving of journal articles on the web and in Open Access repositories.
> ```

Different type of access

- Golden OpenAccess

  > ```
  > Gold open access - Gold OA makes the final version of an article freely and permanently accessible for everyone, immediately after publication. Copyright for the article is retained by the authors and most of the permission barriers are removed. Gold OA articles can be published either in fully OA journals (where all the content is published OA) or hybrid journals (a subscription-based journal that offers an OA option which authors can chose if they wish). An overview of fully OA journals can be found in the Directory of Open Access Journals (DOAJ).
  > ```

> TIP: just because a journal offers free access to content this does not mean is it Open Access. As described above Gold OA also allows the re-use of the work as long as the authors are acknowledged and cited as they retain the copyright. Simply allowing everyone with an internet connection to read the content does not constitute gold OA.

![img](Lesson5/142px-Open_Access_logo_PLoS_transparent.svg.png)

[^]: Taken from https://commons.wikimedia.org/wiki/File:Open_Access_logo_PLoS_transparent.svg. This file is made available under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons)[CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en)



- Green OA

  > Green open access - Green OA, also referred to as self-archiving, is the practice of placing a version of an author’s manuscript into a repository, making it freely accessible for everyone. The version that can be deposited into a repository is dependent on the funder or publisher. Unlike Gold OA the copyright for these articles usually sits with the publisher of, or the society affiliated with, the title and there are restrictions as to how the work can be reused. There are individual self-archiving policies by journal or publisher that determine the terms and conditions e.g. which article version may be used and when the article can be made openly accessible in the repository (also called an embargo period). A list of publishers’ self-archiving policies can be found on the SHERPA/RoMEO database.

### Licence

The whole business of papers is based on some core concept that protect Intellectual Properties (IP). One of this is licence. It is crucial to RESPECT licensing and recognise authors of the work we are using. For this reason along any image, in this lectures I tried at my best to indicate the source and the type of licence that underlies each image I used. 

> A license can be granted by a party to another party as an element of an agreement between those parties. A shorthand definition of a license is "an authorization to use licensed material." 

# Creative commons

![CC](Lesson5/1024px-CC-logo.svg-1616503155992.png)

[^]: Taken from https://commons.wikimedia.org/wiki/File:CC-logo.svg . *This logo image consists only of simple geometric shapes or text. It **does not meet the [threshold of originality](https://commons.wikimedia.org/wiki/Commons:Threshold_of_originality)** needed for copyright protection, and is therefore in the **[public domain](https://en.wikipedia.org/wiki/public_domain)**. Although it is free of copyright restrictions, this image may still be subject to [other restrictions](https://commons.wikimedia.org/wiki/Commons:Non-copyright_restrictions). See [WP:PD#Fonts and typefaces](https://en.wikipedia.org/wiki/Wikipedia:Public_domain#Fonts_and_typefaces) or [Template talk:PD-textlogo](https://commons.wikimedia.org/wiki/Template_talk:PD-textlogo) for more information.*  [![Trademarked](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Trademark_Warning_Symbol.svg/64px-Trademark_Warning_Symbol.svg.png)](https://commons.wikimedia.org/wiki/Commons:Non-copyright_restrictions) This work includes material that may be **protected as a [trademark](https://en.wikipedia.org/wiki/en:trademark)** in some jurisdictions. If you want to use it, you have to ensure that  you have the legal right to do so and that you do not infringe any  trademark rights. See our [general disclaimer](https://commons.wikimedia.org/wiki/Commons:General_disclaimer).

As the foot note of the previous logos shows, sometimes things are not as straightforward as expected. So please be careful! Misbehaviour in this, for example uploading something on ArXiv in the wrong manner can lead to rejection of publishing for papers. 





> Creative Commons is a nonprofit organization dedicated to building a globally-accessible public commons of knowledge and culture. We make it easier for people to share their creative and academic work, as well as to access and build upon the work of others. By helping people and organizations share knowledge and creativity, we aim to build a more equitable, accessible, and innovative world.



*The idea behind licensing is not to **HIDE** something. It is the EXACT OPPOSITE!*

![CC2](Lesson5/Free-cultural-license-cc.svg-1616503148391.png)

[^]: Taken from https://commons.wikimedia.org/wiki/File:Free-cultural-license-cc.svg . This file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons) [Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/deed.en) license.

The idea behind CC is the concept of [copyleft](https://en.wikipedia.org/wiki/Copyleft)

As you can read from [here](https://en.wikipedia.org/wiki/File:Copyleft.svg) the copyleft logo is on the public domain and you do not need to mention anything

![CL](im_l5/240px-Copyleft.svg.png)

Copyleft is about freedom:

- Freedom 0 :        **the freedom to use the work**
- Freedom 1 :         the freedom to study the work
- Freedom 2 :         the freedom to copy and share the work with others
- Freedom 3 :         the freedom to modify the work, and the freedom to distribute modified and therefore derivative works

![Freedom](im_l5/967px-CC_guidant_les_contributeurs.jpg)

[^]: Taken from https://commons.wikimedia.org/wiki/File:CC_guidant_les_contributeurs.jpg. This file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/en:Creative_Commons) [Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.



[Notes for Digital-Age tools for researcher ](https://gitlab.com/cippo1987/DigitalageTools) © 2021   by  [ Federico Brivio ](https://gitlab.com/cippo1987/) is licensed under [ CC BY-NC 4.0 ![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxxJREFUSIm11luIVlUUB/DfzKhTwQQW2nQhiHwJAiOYyKjEKGhSoyCyh4QIulHQjSBf6sGp6AKVURBERUlJBBH1EPSQhVA9dBG6aPegtBG6TdNUjs7Xw97Hs77t+Zzv0/EPh3PO3v+9/nuvtfdau093OBIrsBpLcRyG0cIvGMeneBOb8U+XdjtiGI9jMot080ziCZxwsKJr8UeD4b/wJd6VVvcFJhp4E7iuF8E+3F8Y+ROP4HzMaxgzgHPxcMNkn8L8bkRfDINm8CQW9TDxY/Eo9gY7b80mfm8g/4urexAsMap99c92Il4mrbCF/7D8EEQrnKE9/teXhCPwYyD0tClmwSj2ZLu/Y2HsvD2IvjSHohU2BPtjVeOAlAQqF59yGIQXq10+gcF5WCZlItiI74tBi3ANTscUPsImKVFUOBNrpKSxS9rFb4f+XdLmuhVD8v55SO2G0UJ0GX61f3L4BksyZ516U8ZnE/qDrRWh7zFSBmrhbwwG4lHYoU6D9+Au/JyF1uPCYGwrbpLiuUdy6dnB3nx16t0CX+Wf7cVqrwpGbwvtSzGSv1/J/dM4KXAu1Zx0tmf+16Tc28I7BWl9EB7RjM/Vru8GmzN/MsagryC1wne/ZrRm6S9Racz0Y2f+Ob4gRddfEL5PxLVSsdiW207GqYGzSnt8KyzO73F4L898SspgFYakY1Dl7THcgM9y29NYqQ7HNtws5fup3HZRsDegPstbSKWsGryymOFyKc2VR+UD9ea5r6F/RiqtEeeE/g1wXmh4roN77sTzUolcY/8SN4IH8AIelBJKiVjjL65cMJ4bdqsTw1xiIX5T32D2hfSOMJtXD4NwDGdbCBbg29B5yxyKlmXxmJKwSn1dmc7/h4qztF8EbuxEXBdI01I1OVisle7Xlb2NByL34RntR+N1nNaD4BKpMkUbr0nhnBV3a78lTuNlXImjG/hDuEJa1W7t53lM9ykVXILvipm38oR24hN8LJXOvQ28H3B5L4IRC6SSWF2Nunl25DGDDfb2oaxInTAg3UZWS8l/WCoWLfyUJ/Y+3sCHkosPiP8Btp0swL9OhkEAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAArlJREFUSIm11k9oHVUUBvBfX2Ka11TbGkhaWwPu3FmoQiBBsXShNEEEC9KFkIVIN64sKAUXhVaolJZIIkZB6ap0kYUKpSstrbpw4UJqu1RjSlOMljTEpK2NizvDu5k3M5kXkw8uM9z7nfPdc/+cezaphjr2YwjPoBc7sYxbmMENfIMvMVfRbyG24wPMJyJV2gLGsWetoiOYzXE8j+u4jG/xixBhHu9IK4JtOJNxMofTeAHtBTaDOIW/M7af4JHVRGuYjIweCsvW08LEu5OJ/xv5ubSa+KmI/A9eL+A9i8+Ttq+AM2zlFnxcJHo4Ii0Ky5qHTbgZcaeTvjzszYi/lSVsFa5FShgpmh0e03yQHi3hD2ss+xx2x4PvR07OlThJcS3i/1yBHx/Wj9LODtzRuINPVnDUg7eTVuXgdUca86i3C3u5LSF8gakSB8fxdKZvMPn+JCSbPMziU7yDLrwIoxrLcKBEtK55b7OtXmL/fMQbg6saS9BRYthVQbirxL4ddxPedzXsSgamcK/E8P/igXD1oLcmvDSE67TRSDV21oTQKU4C64lUY7kmvKWE93Wjka7uTE0j/D3KE/mSkPImcsYmkrGlEvs2PJH834azql2nFIc0n+ZDFewGIv54DV9Hg69WcLBWDEf/Fwl39y+Np7BvFQdrifjxSGMBW2rC3f0wIXTiZNUQWsAx7Ej+xxJxhFT3u0YEZbVSqxEfFJLHslAWdWcJr2i8m/fw8joID1hZob5ZNLtjEem+/Mj7cSHT+nN4bwhnJvU3WiRKyCyfZaI5j6fKjDLoE4qJ2Mek/Oq0SfxdK6vEReE9PSj/6duMl4RSNo7yIU4I1WtlDOE3zfu5mPT/gO/xa0YsbX/gtVYEY3TiKP7McVzUZvEetpQ5rvoitQsVxBCeE5J9mnenhdz7I77CFeH6lOI/baALunnzplQAAAAASUVORK5CYII=)![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAbrAAAG6wFMMZ5KAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxRJREFUSIml1luoZ3MUB/DPueaMQ0PkKLc8eOWBooTIA2eGkii8jHKb4kGMePCiyWAkU2Z4QEnydMp40ESMa4ohI3XSKJcZuR3jcsxw5jh/D2vv8//tfX577/+MVb/6//de67vWXr/vugwZTCZwKdbgbJyEKfTwA37ELN7CdvwxIG6jrMbDmC+cDHIOYCtOOVKn6zCXAZ7DbuzAa/gMP2f05nHH4TgcwRM1kN+wCedjOGMzhPPwEH6p2T6DsS6nw5hJjJbwJE44jMBX4xEsJjg7upw/migfxHUtulM4Uz4DcCV+T/CebgK6IVH6Gxe1RYhXCt3JFp1zBMNL3BV3PinKolRY1+F0UMcwrZ/2P9XY/mDi9Fdcj9EMyDCuwcuFXg/v4HFR302yOcF/qnw4LlhbL4evcTeOLfRGCodN9buEexocH58E+pdoSC5vAesJgmzGhtrzb/BuAvg5Tmz56k2J7TRsqT3YgO86gnk+ARzHraKNtsmFif1WeE//4staGxMs/7jB8SyuFekfVEYKHz28D3uKP7sbDC4Rjf/fTABf4S7dzC5ltrDbI4nizQ6js0SKcgNjv2g+p3Zg7NTP7rLjnQNGfRzuw95MAAt4Cec22L6uP0CWUz07oONSxnETPs0E0MPbuFq1pe7Sv6Jlcs3rniI5Mh0jf//l+RLrBQ/K7H5AdQRe1uJ0leDBNtV6vSCxX8Jz8jxIB8Y2YqVZ0c5qMoE3Er1DIsUfFr/T8Uc7D3pYS6S33DQO4rSM41FsxD8NQD18YWUTGcON+CTRO1BkD9V2+GLDV8PpeEBMpnLTmMFtgmxNkg6Jx9IXE/g2eXl7C0gp2wvdozv0rtAfi/vFwKjIGn12LhQG/9fxxapEu6VJMU35IdzZAjopSDTU8P5mVU5sacFCUD0lzYxol4PKGaJ71TFyi8UKuVe1KSzgBbF95AbCKlyFZ8W+ltb1Rs0LYVamxbCvl80ivhd1vAv7VFfZ8uwVo/OI5Chx7/Ulve3M4X5JreakiRh1GRXr7loxeaZwcvFuH37CR3hVrEOLXYD/AYTjaOgDscSLAAAAAElFTkSuQmCC)](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)

Your rights are available [here](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1).
